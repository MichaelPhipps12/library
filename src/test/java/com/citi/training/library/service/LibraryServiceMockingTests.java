package com.citi.training.library.service;

import static org.mockito.Mockito.when;

import com.citi.training.library.dao.LibraryMongoRepo;
import com.citi.training.library.model.LibraryItem;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;

@SpringBootTest
public class LibraryServiceMockingTests {

    @Autowired
    private LibraryService libraryService;

    @MockBean
    private LibraryMongoRepo mockLibraryRepo;  //MADE FAKE REPO

    @Test
    public void test_libraryService_save(){
        LibraryItem testItem = new LibraryItem();
        testItem.setType("DVD");
        testItem.setBorrowed(true);

        // Tell mockRepo that libraryService is going to call save()
        // when it does return the testItem object
        when(mockLibraryRepo.save(testItem)).thenReturn(testItem);
        libraryService.save(testItem);
        // Told fake repo "something is about to call your save method
        // I want you to return the test employee"
    }
    
}
