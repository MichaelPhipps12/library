package com.citi.training.library.service;

import com.citi.training.library.model.LibraryItem;
import org.junit.jupiter.api.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest
public class LibraryServiceTests {

    private static final Logger LOG = LoggerFactory.getLogger(LibraryServiceTests.class);

    @Autowired
    private LibraryService libraryService;

    @Test
    public void test_save_sanityCheck() {

        LOG.debug("This is a message");

        LibraryItem testItem = new LibraryItem();
        testItem.setType("CD");
        testItem.setBorrowed(true);

        libraryService.save(testItem);
    }

    @Test
    public void test_findAll_sanityCheck(){
    assert(libraryService.findAll().size() > 0);
    }
}