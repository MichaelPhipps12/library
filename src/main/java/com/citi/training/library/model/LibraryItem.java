package com.citi.training.library.model;

public class LibraryItem {
    
    private String type;
    private boolean borrowed;

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public boolean getBorrowed() {
        return borrowed;
    }

    public void setBorrowed(boolean borrowed) {
        this.borrowed = borrowed;
    }

    @Override
    public String toString() {
        return "Library [type=" + type + ", borrowed=" + borrowed + "]";
    }
}
