package com.citi.training.library.rest;

import com.citi.training.library.service.LibraryService;

import java.util.List;

import com.citi.training.library.model.LibraryItem;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/v1/libraryitem")
public class LibraryController {
    
    // Write functions to accept requests

    private static final Logger LOG = LoggerFactory.getLogger(LibraryController.class);

    @Autowired
    private LibraryService libraryService;

    @RequestMapping(method = RequestMethod.GET)
    public List<LibraryItem> findAll() {
        LOG.debug("findAll() request received"); //Wouldn't put info because too much will be in the log
        return libraryService.findAll();
    }

    @RequestMapping(method=RequestMethod.POST)
    public ResponseEntity<LibraryItem> save(@RequestBody LibraryItem libraryItem) {
        LOG.debug("save item request received");

        return new ResponseEntity<LibraryItem>(libraryService.save(libraryItem),
                                            HttpStatus.CREATED);
    }

}
